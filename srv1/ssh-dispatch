#!/usr/bin/ruby
require_relative "../lib/gems"
require_relative "../lib/defs"
require_relative "../lib/common"

x = ENV["SSH_ORIGINAL_COMMAND"]

unless x
  puts "ssh-dispatch: fatal: only specific commands allowed"
  exit 100
end

unless zfs_home_mounted?(getlogin)
  puts "ssh-dispatch: fatal: home ZFS appears not to be mounted"
  exit 100
end

y = x.split(" ")

Dir.chdir(home_dir)
File.umask(0077)

if x == "/usr/lib/openssh/sftp-server"
  exec("/usr/lib/openssh/sftp-server", "-u", "0077")
elsif x == "__quota"
  puts zfs_get("used", "pool1/home/#{getlogin}") + " of " + zfs_get("quota", "pool1/home/#{getlogin}")
elsif x == "__passwd"
  exec("/usr/bin/passwd")
elsif x == "__add_key"
  key = $stdin.read
  if key.start_with?("ssh-")
    FileUtils.mkdir_p("#{home_dir}/.ssh")
    append_file("#{home_dir}/.ssh/authorized_keys", key)
    puts "ssh-dispatch: success: key added to authorized_keys"
  else
    puts "ssh-dispatch: fatal: this does not look like an SSH key"
    exit 100
  end
elsif x == "__list_keys"
  authorized_keys_file = "#{home_dir}/.ssh/authorized_keys"
  if File.exist?(authorized_keys_file)
    File.readlines(authorized_keys_file).map(&:chomp).each_with_index do |key, i|
      puts "#{i}: #{key}"
    end
  end
elsif y[0] == "__remove_key"
  unless y.length == 2 && y[1].match(/\A[0-9]+\z/)
    puts "ssh-dispatch: fatal: exactly one non-negative integer argument required"
    exit 100
  end
  to_remove = y[1].to_i
  authorized_keys_file = "#{home_dir}/.ssh/authorized_keys"
  if File.exist?(authorized_keys_file)
    current_keys = File.readlines(authorized_keys_file).map(&:chomp)
    unless to_remove < current_keys.length
      puts "ssh-dispatch: fatal: no such key"
      exit 100
    end
    remaining_keys = current_keys.select.with_index do |_, i|
      i != to_remove
    end
    if remaining_keys.empty?
      FileUtils.rm(authorized_keys_file)
    else
      via_tempfile(authorized_keys_file) do |t|
        t.write(remaining_keys.join("\n") + "\n")
      end
    end
    puts "ssh-dispatch: success: removed key with index #{to_remove}"
  else
    puts "ssh-dispatch: fatal: no authorized_keys file found"
    exit 100
  end
elsif y[0] == "__git_init"
  unless y.length == 2
    puts "ssh-dispatch: fatal: exactly one argument required"
    exit 100
  end
  unless File.exists?(y[1])
    FileUtils.mkdir_p(File.dirname(y[1]))
    system_ok("git", "init", "--bare", y[1])
    FileUtils.chmod(0700, y[1])
    puts "ssh-dispatch: success: bare git repository created"
  else
    puts "ssh-dispatch: fatal: file or directory already exists"
    exit 100
  end
elsif x.start_with?("git-")
  exec("/usr/bin/git-shell", "-c", x)
else
  puts "ssh-dispatch: fatal: this command is not allowed"
  exit 100
end
