# coding: utf-8
MY_NAMESPACE = "de.uni_kiel.math"
MY_NAMESPACE_ = "de_uni_kiel_math"
MY_NAME = "fatclient-system"
MY_REPOS = "/opt/#{MY_NAMESPACE}/#{MY_NAME}"
MY_ETC = "/etc/opt/#{MY_NAMESPACE}/#{MY_NAME}"
MY_CLIENT_PACKAGE_LIST = "#{MY_REPOS}/client/packages.list"
MY_CLIENT_FLATPAK_LIST = "#{MY_REPOS}/client/flatpak.list"
MY_DEPENDS = File.readlines(__dir__ + "/my_depends").map(&:chomp)
CN_ADMIN = "EDV Mathematik"
EMAIL_ADMIN = "edv@math.uni-kiel.de"
HOST_SRV1 = "srv1-fc.math.uni-kiel.de"
HOST_SRV1_PUBLIC_NAME = "srv1.math.uni-kiel.de"
HOST_SRV1_USERDIR_NAME = "user.math.uni-kiel.de"
IP_ADDRESS_SRV1 = "172.19.104.1"
IP_ADDRESS_SRV1_PUB = "134.245.104.1"
USER_DIS = "cltdis"
USER_LOG = "cltlog"
USER_FTP = "cltftp"
USER_GIT = "cltgit"
USERS_CLT = [USER_DIS, USER_LOG, USER_FTP, USER_GIT]
SSH_DIS = "#{USER_DIS}@#{HOST_SRV1}"
SSH_LOG = "#{USER_LOG}@#{HOST_SRV1}"
SSH_FTP = "#{USER_FTP}@#{HOST_SRV1}"
SSH_GIT = "#{USER_GIT}@#{HOST_SRV1}"
SUBNETS_PRIV = ["172.19.104.0/25", "172.19.104.128/25"]
SUBNETS_PRIV_TRUSTED = ["172.19.104.0/25"]
SUBNETS_PROT = ["134.245.104.0/24", "134.245.106.0/24", "172.19.104.0/25"]
NAMESERVERS = ["134.245.10.7", "134.245.1.36", "134.245.1.2"]
DEVICE_EXTERN = "ens192"
DEVICE_SUBNET_ANTISPOOF = [
  { device: "ens256", subnet: "172.19.104.128/25" }
]
SUBNET_UNI = "134.245.0.0/16"
SUBNET_MATH_104 = "134.245.104.0/24"
SUBNET_MATH_106_1 = "134.245.106.0/25"
SUBNET_MATH_106_2 = "134.245.106.128/25"
SUBNET_INF_248 = "134.245.248.0/24"
SUBNET_INF_253 = "134.245.253.0/24"
SUBNET_SEMINAR = "192.168.128.0/17"
LDAP_DOMAIN = "math.uni-kiel.de"
LDAP_DOMAIN_DN = "dc=math,dc=uni-kiel,dc=de"
LDAP_ORG = "math"
LDAP_ROUNDS = 1000000
LDAP_PRIMARY_GROUP = { groupname: "ldap.math.uni-kiel.de", gidNumber: 100000 }
LDAP_LOCKED_GROUP = { groupname: "locked.math.uni-kiel.de", gidNumber: 100100 }
LDAP_GROUPS = [ { groupname: "staff.math.uni-kiel.de",    gidNumber: 100001 },
                { groupname: "student1.math.uni-kiel.de", gidNumber: 100002 },
                { groupname: "student2.math.uni-kiel.de", gidNumber: 100003 },
                { groupname: "guest1.math.uni-kiel.de",   gidNumber: 100004 },
                { groupname: "guest2.math.uni-kiel.de",   gidNumber: 100005 },
              ]
LDAP_GROUPS_NAMES = LDAP_GROUPS.map{ |g| g[:groupname] }
LDAP_GROUPS_NAMES_EXTERNAL_SFTP = [ "staff.math.uni-kiel.de",
                                    "student2.math.uni-kiel.de",
                                    "guest2.math.uni-kiel.de",
                                  ]
LDAP_UIDNUMBER_OFFSET = 100000
LDAP_UIDNUMBER_MAX = 2147483647 # maximum that we ever intent to use
HOST_SRV2 = "srv2.math.uni-kiel.de"
HOST_SRV2_OFFICIAL_NAME = "service.math.uni-kiel.de"
IP_ADDRESS_SRV2 = "134.245.104.2"
SMIME_PRIVKEY_FILE = "/etc/ssl/private/de.uni_kiel.math.EdvEmail.pem"
SMIME_CERT_FILE = "/etc/ssl/certs/de.uni_kiel.math.EdvEmail.pem"
USERDB_SYSTEMD = "#{MY_NAMESPACE}.ProcessUserdb"
USERDB_ACTIVATION_PERIOD = 3.days
USERDB_RENEWAL_PERIOD = 1.year
USERDB_GRACE_PERIOD = 3.months
USERDB_REMINDER_PERIOD = 2.weeks
USERDB_COPY_RETENTION = 3.days
USERDB_TEST_USERS = ["lxkl"]
USERDB_EMAIL_TEXT = <<_EOT_
Content-type: text/plain; charset=UTF-8

Greetings,

your account <%= email_text_username %> at the Department of Mathematics
at Kiel University needs to be <%= email_text_action_1["en"] %>.

Please follow this link to have your account <%= email_text_action_2["en"] %>:

https://#{HOST_SRV2_OFFICIAL_NAME}/it/en/<%= email_text_action %>/<%= email_text_id %>/<%= email_text_code %>

This link does only work inside of the campus network.

This message was sent at <%= email_text_now %>.
Any such link that you may have received earlier is no longer valid.

Any questions can be sent as a reply to this e-mail.

Thank you.

Lasse Kliemann <l.kliemann@math.uni-kiel.de> / Tel. 1155
Jörg Rieder <rieder@math.uni-kiel.de> / Tel. 4394

================================================================================

Guten Tag,

Ihr Konto <%= email_text_username %> am Mathematischen Seminar
der Christian-Albrechts-Universität zu Kiel muss <%= email_text_action_1["de"] %> werden.

Bitte folgen Sie diesem Link, um Ihr Konto zu <%= email_text_action_2["de"] %>:

https://#{HOST_SRV2_OFFICIAL_NAME}/it/de/<%= email_text_action %>/<%= email_text_id %>/<%= email_text_code %>

Dieser Link funktioniert nur innerhalb des Campus-Netzwerkes.

Diese Nachricht wurde verschickt um <%= email_text_now %>.
Etwaige Links, die Sie früher erhalten haben, sind nicht mehr gültig.

Bei Fragen antworten Sie gerne auf diese E-Mail.

Vielen Dank.

Lasse Kliemann <l.kliemann@math.uni-kiel.de> / Tel. 1155
Jörg Rieder <rieder@math.uni-kiel.de> / Tel. 4394
_EOT_
USERDB_ERROR_MESSAGE_CODE = { "en" => "Unknown user, or wrong or expired code.",
                              "de" => "Unbekannter User oder falscher oder abgelaufener Code." }
USERDB_DEFAULT_GROUP_NAME_REGISTER = "student1.math.uni-kiel.de"
USERDB_DEFAULT_GROUP_NAME_ADDUSER = "guest1.math.uni-kiel.de"
QUOTA_DEFAULT = { "staff.math.uni-kiel.de"    => { block: 50, inode: 10000 },
                  "student1.math.uni-kiel.de" => { block:  1, inode: 10000 },
                  "student2.math.uni-kiel.de" => { block:  1, inode: 10000 },
                  "guest1.math.uni-kiel.de"   => { block:  1, inode: 10000 },
                  "guest2.math.uni-kiel.de"   => { block:  1, inode: 10000 },
                }
USER_SRV2_USERDB = "userdb"
USERDB_RECORD_DISPLAY = [
  { attribute: "id", title: "Id", escape: false },
  { attribute: "_username", title: "Username", escape: false },
  { attribute: "_first_name", title: "First name", escape: true },
  { attribute: "_last_name", title: "Last name", escape: true },
  { attribute: "_email", title: "E-mail", escape: false },
  { attribute: "_groupname", title: "Groupname", escape: false },
  { attribute: "_quota_block", title: "Block quota", escape: false },
  { attribute: "_quota_inode", title: "Inode quota", escape: false },
  { attribute: "_valid_for", title: "Valid for", escape: false },
  { attribute: "_created_at", title: "Created at", escape: false },
  { attribute: "_renewed_at", title: "Renewed at", escape: false },
  { attribute: "_barred", title: "Barred?", escape: false },
  { attribute: "_activated", title: "Activated?", escape: false },
]
URL_TERMS_OF_USE = "https://cloud.rz.uni-kiel.de/index.php/s/HyssEiKCAP5w4Q7"
URL_INFO = "https://cloud.rz.uni-kiel.de/index.php/s/dt8a6oEgwjyB722"
URL_LEGAL_NOTICE = "https://user.math.uni-kiel.de/~lxkl/LegalNotice.html"
SSHD_DISPATCH = <<_EOT_
  ForceCommand #{MY_REPOS}/srv1/ssh-dispatch
  PermitTunnel no
  AllowAgentForwarding no
  AllowTcpForwarding no
  X11Forwarding no
_EOT_
SSHD_INTERNAL_SFTP = <<_EOT_
  ForceCommand internal-sftp
  PermitTunnel no
  AllowAgentForwarding no
  AllowTcpForwarding no
  X11Forwarding no
_EOT_
DAILY_SNAPSHOT_SPEC = [ { tag: "daily", retention: 5 },
                        { tag: "monthly", retention: 6 },
                      ]
DIR_CRON = "/root/must-config-all-cron.run"
FLOCK_CRON = "/root/must-config-all-cron.flock"
KEY_ID = "FB57DD3E568C11C5"
DIR_MAPLESOFT = "/opt/maplesoft"
VERSION_MAPLE_LMGRD = "11.13.1.2"
URL_MAPLE_LMGRD = "http://www.maplesoft.com/downloads/?d=E9E9EB89AE7CA21FD284839E0BD53958&v=10&f=NetworkToolsLinuxX64Installer.run"
FILE_MAPLE_LIC = "Maple2019.lic"
KEYID_SKYPE = "1F3045A5DF7587C3"
KEYID_VSCODE = "EB3E94ADBE1229CF"
KEYID_MEGASYNC = "03C3AD3A7F068E5D"
DEB_BROTHER_BRSCAN_5 = "brscan5-1.2.6-1.amd64.deb"
## printers:
##  21: Epson WorkForce Pro WF-4640 in 01.039
##  61: Kyocera Ecosys FS-4000DN in 01.040
##  62: Lexmark C734dn in 01.040
## 100: Brother HL-L6400DW in 01.001
## 103: Lexmark MS510dn in 02.029
## 230: Brother HL-L6400DW in 01.040
## 231: Brother HL-L6400DW in 02.029
DEBS_BROTHER_HLL6400DW = [ "hll6400dwcupswrapper-3.5.1-1.i386.deb", "hll6400dwlpr-3.5.1-1.i386.deb" ]
IPLOC_BROTHER_HLL6400DW = [ { ip: "134.245.104.100", location: "01.001" },
                            { ip: "134.245.104.230", location: "01.040" },
                            { ip: "134.245.104.231", location: "02.029" } ]
PROFILE_DEF = {
  "default" => {
    gateway: "172.19.104.1",
    subnet_prefix: "172.19.104",
    subnet_suffix_min: 11,
    subnet_suffix_max: 125,
    subnet_mask_number: "25",
    ip_config_temp: "172.19.104.10/25",
    config_printers: true,
    default_printer: "01.040_Brother_HLL6400DW"
  },
  "pcpool" => {
    gateway: "172.19.104.129",
    subnet_prefix: "172.19.104",
    subnet_suffix_min: 131,
    subnet_suffix_max: 252,
    subnet_mask_number: "25",
    ip_config_temp: "172.19.104.130/25",
    config_printers: false,
    default_printer: nil
  }
}
