def mkcert(host, cn, service, user)
  cakey = "/etc/ssl/private/#{host}_cakey.pem"
  cacert = "/etc/ssl/certs/#{host}_cacert.pem"
  cacertsys = "/usr/local/share/ca-certificates/#{host}_cacert.crt"
  tmpinfo = "/etc/ssl/{tmp}.info"
  unless File.exist?(cacert)
    apt_get_install("gnutls-bin", "ssl-cert")
    system_ok("certtool", "--generate-privkey", "--outfile", cakey)
    FileUtils.chmod(0600, cakey)
    File.write(tmpinfo, <<_EOT_)
cn = CA for #{host}
ca
cert_signing_key
_EOT_
    system_ok("certtool", "--generate-self-signed", "--load-privkey", cakey,
              "--template", tmpinfo, "--outfile", cacert)
    FileUtils.rm(tmpinfo)
    FileUtils.cp(cacert, cacertsys)
    system_ok("update-ca-certificates")
  end
  hostkey = "/etc/ssl/private/#{cn}_hostkey_#{service}.pem"
  hostcert = "/etc/ssl/certs/#{cn}_hostcert_#{service}.pem"
  system_ok("certtool", "--generate-privkey", "--outfile", hostkey)
  FileUtils.chown("root", "ssl-cert", hostkey)
  FileUtils.chmod(0640, hostkey)
  system_ok("gpasswd", "-a", user, "ssl-cert")
  File.write(tmpinfo, <<_EOT_)
organization = #{service} at #{cn}
cn = #{cn}
tls_www_server
encryption_key
signing_key
expiration_days = 3650
_EOT_
  system_ok("certtool", "--generate-certificate", "--load-privkey", hostkey,
            "--load-ca-certificate", cacert, "--load-ca-privkey", cakey,
            "--template", tmpinfo, "--outfile", hostcert)
  FileUtils.rm(tmpinfo)
  yield(cacert, hostkey, hostcert)
end

def ldapdopw(action, password, content)
  pipe_into(content, "ldap#{action}", "-x", "-D", "cn=admin,#{LDAP_DOMAIN_DN}", "-w", password)
end

def ldapdoext(action, content)
  pipe_into(content, "ldap#{action}", "-Y", "EXTERNAL", "-H", "ldapi:///")
end

def setup_postfix(host)
  debconf("postfix	postfix/main_mailer_type	select	Internet Site")
  debconf("postfix	postfix/mailname	string	#{host}")
  apt_get_install("mailutils", "postfix")
  set_option_equals_spaces("/etc/postfix/main.cf", "inet_interfaces", "loopback-only")
  set_option_equals_spaces("/etc/postfix/main.cf", "mydestination", "$myhostname")
  systemctl_restart("postfix.service")
end

def config_apache
  ["cgi", "dav", "dav_fs", "dav_lock"].each { |m| system_ok("a2dismod", m) }
  ["ssl", "rewrite"].each { |m| system_ok("a2enmod", m) }
  ["default-ssl", "000-default"].each { |s| system_ok("a2dissite", s) }
end

def setup_fail2ban
  apt_get_install("fail2ban")
  systemctl_enable("fail2ban.service")
  File.write("/etc/fail2ban/jail.d/customisation.local", <<_EOT_)
[DEFAULT]
maxretry = 20
_EOT_
end

# want_testing skips a few steps that are or could be a problem for
# systems not running on a VM at RZ
def setup_debian_server(host, dir, want_testing: false)
  ##
  ## etckeeper
  setup_etckeeper
  ##
  ## nameserver
  unless want_testing
    File.write("/etc/resolv.conf", "search math.uni-kiel.de\n")
    NAMESERVERS.each { |ip| append_file("/etc/resolv.conf", "nameserver #{ip}\n") }
  end
  ##
  ## prevent any shutting off by itself
  gdm_config_file = "/etc/gdm3/greeter.dconf-defaults"
  if File.exist?(gdm_config_file)
    set_option_equals(gdm_config_file, "sleep-inactive-ac-type", "'blank'")
    systemctl_restart("gdm.service")
  end
  system_ok("systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target")
  systemctl_restart("systemd-logind.service")
  ##
  ## server tools
  install_server_tools
  ##
  ## disable services not needed at the moment
  ["avahi-daemon.service", "cups.service", "cups-browsed.service"].each do |s|
    systemctl_disable(s) if systemctl_exists?(s)
  end
  ##
  ## secure root account and sshd
  uncomment_line("/etc/pam.d/su", /auth\s+required\s+pam_wheel\.so$/)
  userdel("toor")
  setup_fail2ban
  ##
  ## secure grub
  secure_grub
  ##
  ## postfix and mail alias
  setup_postfix(host)
  add_mail_alias("root", EMAIL_ADMIN)
  ##
  ## iptables
  systemd_add_service("#{MY_NAMESPACE}.Iptables", "#{MY_REPOS}/#{dir}/config-iptables")
  ##
  ## misc
  config_shell_profile
  config_unattended_upgrades
  config_timeserver unless want_testing
end
