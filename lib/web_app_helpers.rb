# coding: utf-8
module WebAppHelpersCommon
  def pipe_into(content, *command)
    if command.length == 1; command = command[0] end
    res = nil
    IO.popen(command, "r+") do |io|
      io.write(content)
      io.close_write
      res = io.read.chomp
      io.close
    end
    unless $?.success?
      puts "fatal: unable to pipe into: #{command}"
      exit 111
    end
    res
  end

  def home_dir(user=nil)
    if user
      Etc.getpwnam(user).dir
    else
      Etc.getpwuid(Process.uid).dir
    end
  end

  def cd_home_dir
    Dir.chdir(home_dir)
  end

  def getlogin(user=nil)
    if user
      Etc.getpwnam(user).name
    else
      Etc.getpwuid(Process.uid).name
    end
  end

  def randompw(len: 16)
    # omit similar chars: i-j, l-1, I-1, j-J, O-0
    # omit y and z, since they are in different locations on US and German keyboards
    chars = ["a", "b", "c", "d", "e", "f", "g", "h", "k",      "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x",
             "A", "B", "C", "D", "E", "F", "G", "H", "K", "L", "M", "N",      "P", "Q", "R", "S", "T", "U", "V", "W", "X",
             "2", "3", "4", "5", "6", "7", "8", "9",
             "$", "%"]
    n = chars.length
    len.times.collect { chars[SecureRandom.random_number(n)] }.join
  end

  def cryptpw(password, salt: nil)
    extra_args = []
    extra_args = ["-S", salt] if salt
    res = pipe_into("#{password}\n", "mkpasswd", "-m", "sha-512", "-R", LDAP_ROUNDS.to_s, "-s", *extra_args)
    "{CRYPT}" + res
  end
end

module WebAppHelpers
  def h(text)
    Rack::Utils.escape_html(text)
  end

  def a(url, text)
    "<a href=\"#{url(url)}\">#{text}</a>"
  end

  def mailto_admin(text=nil)
    unless text
      text = lang("system administration", "Systemadministration")
    end
    "<a href=\"mailto:#{EMAIL_ADMIN}\">#{text} (#{EMAIL_ADMIN})</a>"
  end

  def mailto_admin_en
    mailto_admin("system administration")
  end

  def lang_en
    session[:lang] == "en"
  end

  def lang(*args)
    if args.length == 1 && args[0].is_a?(String)
      args[0]
    elsif args.length == 1
      args[0][session[:lang]]
    elsif lang_en
      args[0]
    else
      args[1]
    end
  end

  def authorization?(password)
    return false unless password.length <= 64
    x = File.read("#{home_dir(USER_SRV2_USERDB)}/authorization").chomp
    salt = x.split("$")[3]
    y = cryptpw(password, salt: salt)
    x == y
  end

  def first_last_name_valid?(name)
    name.length <= 64 && ! name.match(/[<>&#]/) && ! name.empty?
  end

  def email_valid?(email)
    email.length <= 64 && email.match(/\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/)
  end

  def email_uni?(email)
    domain = email.split("@").last
    dcs = domain.split(".")
    dcs.length > 1 && dcs[-1] == "de" && dcs[-2] == "uni-kiel"
  end

  def username_valid?(username)
    ! username.empty? && username.length <= 32 &&
      username.match(/\A[a-z]+[a-z\d\-_.]*\z/) && ! username.match(/\Amath\d+\z/) &&
      ! File.readlines("#{home_dir(USER_SRV2_USERDB)}/existing-users").map(&:chomp).include?(username)
  end

  def other_input_valid?(str)
    str.length <= 64
  end

  def adduser_edituser_validate_userdata
    [:first_name, :last_name, :email, :username, :quota_block, :quota_inode, :valid_for].each do |kw|
      params[kw].strip!
    end
    params[:email].downcase!
    params[:username].downcase!
    params[:email] = nil if params[:email].empty?
    params[:username] = nil if params[:username].empty?
    [:quota_block, :quota_inode].each do |q|
      unless params[q].empty?
        return "Invalid quota." unless other_input_valid?(params[q])
        params[q] = params[q].to_i
        params[q] = nil if params[q] == 0
      else
        params[q] = nil
      end
    end
    unless params[:valid_for].empty?
      return "Invalid validity period." unless other_input_valid?(params[:valid_for])
      unless params[:valid_for] = ChronicDuration.parse(params[:valid_for])
        return "Could not parse validity period."
      end
    else
      params[:valid_for] = nil
    end
    if ! first_last_name_valid?(params[:first_name])
      "Invalid first name."
    elsif ! first_last_name_valid?(params[:last_name])
      "Invalid last name."
    elsif ! LDAP_GROUPS_NAMES.include?(params[:groupname])
      "Invalid group name."
    elsif params[:email] && (! email_valid?(params[:email]) || ! email_uni?(params[:email]))
      "Invalid e-mail address."
    elsif params[:username] && ! username_valid?(params[:username])
      "Invalid or reserved username."
    else
      true
    end
  end

  def register_validate_userdata
    [:first_name, :last_name, :email].each { |kw| params[kw].strip! }
    params[:email].downcase!
    if ! first_last_name_valid?(params[:first_name])
      { "en" => "Invalid value for first name.", "de" => "Unzulässiger Wert für den Vornamen." }
    elsif ! first_last_name_valid?(params[:last_name])
      { "en" => "Invalid value for last name.", "de" => "Unzulässiger Wert für den Nachnamen." }
    elsif ! email_valid?(params[:email]) || ! email_uni?(params[:email])
      { "en" => "Invalid e-mail address.", "de" => "Unzulässige E-Mail-Adresse." }
    elsif User.find_by("_email" => params[:email])
      { "en" => "A user with this e-mail address is already registered.",
        "de" => "Ein User mit dieser E-Mail-Adresse ist bereits registriert." }
    elsif params[:terms_of_use] != "on"
      { "en" => "You must accept the terms of use (including the data protection statement).",
        "de" => "Sie müssen die Nutzungsbedingungen (inklusive der Datenschutzerklärung) anerkennen." }
    else
      true
    end
  end
end
