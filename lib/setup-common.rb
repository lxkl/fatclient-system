def install_server_tools
  apt_get_install("htop", "stress", "diceware", "pwgen", "net-tools", "dnsutils", "quota", "quotatool",
                  "tree", "gnutls-bin", "gnupg", "ssl-cert", "arp-scan", "nmap", "mutt")
end

def install_virtualbox
  system_ok("apt-key add #{MY_REPOS}/key/A2F683C52980AECF.asc")
  system_ok("apt-key add #{MY_REPOS}/key/54422A4B98AB5139.asc")
  File.write("/etc/apt/sources.list.d/virtualbox.list", <<_EOT_)
deb http://download.virtualbox.org/virtualbox/debian bullseye contrib
_EOT_
  system_ok("apt-get update")
  apt_get_install("virtualbox-6.1")
end

def config_shell_profile
  if File.exist?("/etc/bash.bashrc.d"); return false end
  FileUtils.mkdir_p("/etc/bash.bashrc.d")
  File.write("/etc/bash.bashrc.d/#{MY_NAMESPACE}.Ls.sh", <<_EOT_)
alias ll='ls --color=auto -h -l -A -F'
alias ls='ls --color=auto'
_EOT_
  append_file("/etc/bash.bashrc", <<_EOT_)

for file_ in /etc/bash.bashrc.d/*.sh; do
  if test -r "${file_?}"; then
    source "${file_?}"
  fi
done
_EOT_
end

def config_timeserver
  set_option_equals("/etc/systemd/timesyncd.conf", "NTP", "ts1.rz.uni-kiel.de")
  set_option_equals("/etc/systemd/timesyncd.conf", "FallbackNTP", "ts2.rz.uni-kiel.de")
end

def config_unattended_upgrades
  debconf("unattended-upgrades	unattended-upgrades/enable_auto_updates	boolean	true")
  apt_get_reinstall("unattended-upgrades")
  File.write("/etc/apt/apt.conf.d/50unattended-upgrades", <<_EOT_)
Unattended-Upgrade::Origins-Pattern {
  "origin=*";
}
Unattended-Upgrade::DevRelease "false";
_EOT_
end

def disable_gnome_keyring
  disable_file("/usr/bin/gnome-keyring-daemon")
  disable_file("/usr/share/session-migration/scripts/01_migrate_from_keyring")
end

def disable_gnome_tracker
  Dir.glob("/etc/xdg/autostart/tracker-*.desktop").each do |file|
    append_file_ifhasnotline(file, "Hidden=true")
  end
end

def setup_etckeeper
  apt_get_install("etckeeper")
  set_option_equals("/etc/etckeeper/etckeeper.conf", "AVOID_DAILY_AUTOCOMMITS", "1")
  systemctl_stop("etckeeper.timer")
  systemctl_disable("etckeeper.timer")
end

def grub_mkconfig
  system_ok("grub-mkconfig", "-o", "/boot/grub/grub.cfg")
end

def secure_grub
  hash = grub_password_hash(randompw)
  append_file("/etc/grub.d/40_custom", <<_EOT_)
set superusers="admin"
password_pbkdf2 admin #{hash}
_EOT_
  FileUtils.chmod(0700, "/etc/grub.d/40_custom")
  grub_patch_10_linux
  grub_mkconfig
end

def grub_patch_10_linux
  infile = "/etc/grub.d/10_linux"
  if File.exist?("#{infile}.#{MY_NAMESPACE}~")
    infile = "#{infile}.#{MY_NAMESPACE}~"
  end
  if file_has_match_line?(infile, /^CLASS=".* --unrestricted"$/)
    if infile != "/etc/grub.d/10_linux"
      FileUtils.cp(infile, "/etc/grub.d/10_linux")
    end
  else
    process_file(infile, "/etc/grub.d/10_linux") do |c|
      c.sub!(/^(CLASS=".*)"$/, '\1 --unrestricted"')
    end
  end
  FileUtils.chmod("u+x", "/etc/grub.d/10_linux")
end

def install_maple(version, version_desktop)
  dir = "#{DIR_MAPLESOFT}/maple#{version}"
  tarball = "maple#{version}.tar.xz"
  FileUtils.rm_rf(dir)
  FileUtils.mkdir_p(dir)
  local_tarball = File.join(DIR_MAPLESOFT, tarball)
  FileUtils.rm_f(local_tarball)
  sftp_download_res(tarball, local_tarball)
  system_ok("tar", "--owner=root", "--group=root", "-C", DIR_MAPLESOFT, "-xf", local_tarball)
  FileUtils.chmod_R("go-w", DIR_MAPLESOFT)
  FileUtils.chmod_R("a+rX", DIR_MAPLESOFT)
  FileUtils.rm_f(local_tarball)
  uncomment_line("#{dir}/bin/maple", /USE_SOFTWARE_GL/)
  dir_share = "/usr/local/share/applications"
  FileUtils.mkdir_p(dir_share)
  file_desktop_base = "maple#{version_desktop}.desktop"
  file_desktop = "#{dir}/bin/#{file_desktop_base}"
  if File.exist?(file_desktop)
    FileUtils.rm_f("#{dir_share}/#{file_desktop_base}")
    FileUtils.cp(file_desktop, dir_share)
  end
  FileUtils.chown_R("root", "root", DIR_MAPLESOFT)
  FileUtils.chmod_R("go-w", DIR_MAPLESOFT)
  FileUtils.chmod_R("a+rX", DIR_MAPLESOFT)
end

def config_brother_printers
  IPLOC_BROTHER_HLL6400DW.each do |iploc|
    legacy_name = "HLL6400DW_vendor_driver_#{iploc[:location]}"
    system_try("lpadmin", "-x", legacy_name)
    name = "#{iploc[:location]}_Brother_HLL6400DW"
    unless system_capture("lpstat", "-e").match(/^#{name}$/)
      system_ok("lpadmin", "-p", name, "-E",
                "-v", "lpd://#{iploc[:ip]}/binary_p1",
                "-P", "/usr/share/ppd/brother/brother-HLL6400DW-cups-en.ppd")
    end
  end
end
