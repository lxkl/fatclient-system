class User < ActiveRecord::Base
  validates "_username", uniqueness: true, allow_nil: true
  validates "_email", uniqueness: true, allow_nil: true

  def uidnumber; id + LDAP_UIDNUMBER_OFFSET end
  def username; self["_username"] || "math#{uidnumber}" end
  def groupname; self["_groupname"] end
  def password; self["_password"] end
  def activated?; self["_activated"] end
  def quota_block; self["_quota_block"] || QUOTA_DEFAULT[groupname][:block] end
  def quota_inode; self["_quota_inode"] || QUOTA_DEFAULT[groupname][:inode] end
  def quota_block_hard; (1.1 * quota_block).ceil end
  def quota_inode_hard; (1.1 * quota_inode).ceil end
  def first_name; self["_first_name"] end
  def last_name; self["_last_name"] end
  def email; self["_email"] end
  def valid_for; self["_valid_for"] end
  def activation_code; self["_activation_code"] end
  def activation_code_sent?; self["_activation_code_sent"] end
  def manual_activation_code; self["_manual_activation_code"] end
  def renewal_code; self["_renewal_code"] end
  def renewal_code_sent?; self["_renewal_code_sent"] end
  def barred?; self["_barred"] end

  def expired_for
    if valid_for
      Time.now - (self["_renewed_at"] + valid_for)
    else
      Time.now - (self["_renewed_at"] + USERDB_RENEWAL_PERIOD)
    end
  end
  def expired_0?; expired_for > 0 end
  def expired_1?; expired_for > 1 * USERDB_GRACE_PERIOD end
  def expired_2?; expired_for > 2 * USERDB_GRACE_PERIOD end
  def expired_3?; expired_for > 3 * USERDB_GRACE_PERIOD end
  def expired_4?; expired_for > 4 * USERDB_GRACE_PERIOD end

  def activation_missed?; ! activated? && (Time.now - self["_created_at"]) > USERDB_ACTIVATION_PERIOD end
  def activateable?; ! activated? && ! activation_missed? && ! barred? end
  def activation_email_due?; activateable? && ! activation_code_sent? && !!email end
  def renewable?; activated? && ! valid_for && expired_0? && ! expired_2? && ! barred? end
  def renewal_email_due?
    renewable? &&
      (! renewal_code_sent? || (Time.now - self["_renewal_code_sent_at"]) > USERDB_REMINDER_PERIOD) &&
      !!email
  end
  def to_be_locked?; activated? && ((valid_for && expired_0?) || expired_1? || barred?) end
  def to_be_deleted?; activated? && expired_3? end
  def to_be_blanked?; activated? && expired_4? end
  def to_be_enabled?; activated? && ! to_be_locked? && ! to_be_deleted? && ! to_be_blanked? && ! barred? end
end
