require_relative "web_app_helpers"
include WebAppHelpersCommon

## system commands

def system_ok(*command)
  system(*command)
  unless $?.success?
    puts "fatal: unable to: #{command}"
    exit 111
  end
end

def system_try(*command)
  system(*command, out: File::NULL, err: File::NULL)
  $?.success?
end

def system_capture(*command)
  if command.length == 1; command = command[0] end
  IO.popen(command) do |io|
    res = io.read.chomp
    io.close
    unless $?.success?
      puts "fatal: unable to: #{command}"
      exit 111
    end
    return res
  end
end

## execution environment

def abort_if_not_root
  if Process.euid != 0
    puts "Must be run via sudo. Aborting."
    exit 100
  end
end

def as_user(user, chdir: false, &block)
  u = (user.is_a? Integer) ? Etc.getpwuid(user) : Etc.getpwnam(user)
  pid = Process.fork do
    if chdir; Dir.chdir(u.dir) end
    Process::GID.change_privilege(u.gid)
    Process::UID.change_privilege(u.uid)
    block.call
  end
  Process.wait(pid)
  unless $?.success?
    puts "fatal: no success running block as: #{user}"
    exit 111
  end
end

def kill_all(username)
  if system_try("pgrep", "-u", username)
    system_ok("pkill", "-9", "-u", username)
  end
  system_try("pkill", "-9", "-u", username)
end

## files

def via_tempfile(filename)
  if File.symlink?(filename)
    puts "fatal: via_tempfile does not work for symlinks"
    exit 111
  end
  tempfilename = filename + ".tmp." + SecureRandom.hex(32)
  FileUtils.rm_rf(tempfilename)
  FileUtils.touch(tempfilename)
  if File.exist?(filename)
    stat = File.stat(filename)
    FileUtils.chown(stat.uid, stat.gid, tempfilename)
    FileUtils.chmod(stat.mode, tempfilename)
  end
  File.open(tempfilename, "a") do |t|
    yield t
  end
  FileUtils.mv(tempfilename, filename)
end

def append_file(filename, content)
  if File.exist?(filename)
    old_content = File.read(filename)
  else
    old_content = ""
  end
  via_tempfile(filename) do |t|
    t.write(old_content)
    t.write(content)
  end
end

def file_has_match_line?(filename, pattern)
  File.readlines(filename).any? { |line| line =~ pattern }
end

def process_file(filename, to_filename=nil)
  if to_filename.nil?
    to_filename = filename
  end
  content = File.read(filename)
  yield content
  via_tempfile(to_filename) { |t| t.write(content) }
end

def process_file_ln(filename)
  content = File.read(filename)
  via_tempfile(filename) do |t|
    content.each_line { |line| yield t, line.chomp }
  end
end

def append_file_ifhasnotline(filename, line)
  unless File.readlines(filename).map(&:chomp).include?(line)
    append_file(filename, "#{line}\n")
  end
end

def insert_file(filename, direction, location, content)
  process_file_ln(filename) do |f, l|
    if (location.class == Regexp && location.match(l)) || (location.class == String && location == l)
      case direction
      when :before
        f.write(content)
        f.puts(l)
      when :after
        f.puts(l)
        f.write(content)
      end
    else
      f.puts(l)
    end
  end
end

def lockdown(filename)
  f = File.open(filename, File::CREAT)
  if f.flock(File::LOCK_EX | File::LOCK_NB)
    yield
    f.flock(File::LOCK_UN)
  end
  f.close
end

def first_existing_file(*files)
  files.each do |f|
    if File.exist?(f); return f end
  end
  false
end

def remove_old_userdb_copies
  now = Time.now
  Dir.glob("userdb-copy-*-*.sqlite3").each do |fn|
    stamp = fn.split("-")[2].to_i
    FileUtils.rm(fn) if stamp < (now - USERDB_COPY_RETENTION).to_i
  end
end

## sftp

def sftp_stat_ok?(sftp, filename)
  request = sftp.stat!(filename) { |response| return response.ok? }
  request.wait
end

def sftp_file_exists?(host, user, filename)
  Net::SFTP.start(host, user, verify_host_key: :always) do |sftp|
    return sftp_stat_ok?(sftp, filename)
  end
end

def sftp_append_file(host, user, filename, content)
  Net::SFTP.start(host, user, verify_host_key: :always) do |sftp|
    sftp.file.open(filename, "a") { |file| file.write(content) }
  end
end

def sftp_download(host, user, filename_remote, filename_local, *opts)
  system_ok("sftp", *opts, "#{user}@#{host}:#{filename_remote}", filename_local)
end

def sftp_download_res(filename_remote, filename_local)
  sftp_download(HOST_SRV1, USER_FTP, filename_remote, filename_local,
                "-o", "IdentityFile=/root/.ssh/id_rsa_res")
end

def sftp_upload(host, user, filename_local, filename_remote, *opts)
  if filename_remote.include?(" ") || filename_remote.include?("/")
    puts "fatal: sftp_upload: cannot handle filename #{filename_remote}"
    exit 100
  end
  system_ok("scp", *opts, filename_local, "#{user}@#{host}:#{filename_remote}.tmp")
  system_ok("ssh", *opts, "#{user}@#{host}", "mv #{filename_remote}.tmp #{filename_remote}")
end

## git

def git_res(*args)
  system_ok({ "GIT_SSH_COMMAND" => "ssh -o IdentityFile=/root/.ssh/id_rsa_res" }, "git", *args)
end

def git_clone_res(repos_remote, repos_local)
  git_res("clone", "#{SSH_GIT}:#{repos_remote}", repos_local)
end

def git_get_my_repos(prefix: "", branch: nil)
  tmp_repos = "#{prefix}/#{MY_REPOS}.tmp_repos"
  FileUtils.rm_rf(tmp_repos)
  FileUtils.mkdir_p(File.dirname(tmp_repos))
  git_clone_res(MY_NAME, tmp_repos)
  if ! branch
    branch_ = read_my_etc("branch")
  else
    branch_ = branch
  end
  system_ok("git", "-C", tmp_repos, "checkout", branch_)
  git_res("-C", tmp_repos, "pull", "--verify-signatures")
  system_ok("git", "-C", tmp_repos, "verify-commit", "HEAD")
  commit = system_capture("git", "-C", tmp_repos, "rev-parse", "HEAD")
  final_repos = "#{prefix}/#{MY_REPOS}.#{commit}"
  final_name = File.basename(final_repos)
  final_link = "#{prefix}/#{MY_REPOS}"
  if ! File.exist?(final_link) || File.readlink(final_link) != final_name
    FileUtils.rm_rf(final_repos)
    File.rename(tmp_repos, final_repos)
    tmp_link = "#{final_link}.tmp_link"
    FileUtils.rm_f(tmp_link)
    FileUtils.ln_sf(final_name, tmp_link)
    File.rename(tmp_link, "#{final_link}")
  else
    FileUtils.rm_rf(tmp_repos)
  end
end

## system information

def filesystem_uuid(device)
  system_capture("blkid", "-s", "UUID", "-o", "value", device)
end

def mac_address(device)
  mac_address = File.read("/sys/class/net/#{device}/address").chomp
  mac_address.gsub!(":", "-")
  mac_address
end

def mounted?(path)
  File.readlines("/proc/mounts").each do |line|
    if line.split(" ")[1] == path.to_s; return true end
  end
  false
end

def distributor_id
  system_capture("lsb_release -i").split(":")[1].strip
end

def users_list
  res = []
  while pw = Etc.getpwent
    res = res + [pw]
  end
  res
end

## system configuration

def systemctl_active?(unit)
  system "systemctl", "is-active", "--quiet", unit
  $?.success?
end

def systemctl_exists?(unit)
  units = system_capture("systemctl", "--no-legend", "list-unit-files")
  units.match(/^#{unit} /)
end

def systemctl_wait_inactive(unit)
  while systemctl_active?(unit)
    sleep 1
  end
  sleep 1
end

def systemctl_start(unit)
  unless systemctl_active?(unit); system_ok("systemctl", "start", unit) end
end

def systemctl_stop(unit)
  if systemctl_active?(unit); system_ok("systemctl", "stop", unit) end
end

def systemctl_restart(unit)
  system_ok("systemctl", "restart", unit)
end

def systemctl_disable(unit)
  system_ok("systemctl", "disable", unit)
end

def systemctl_kill(unit)
  systemctl_stop(unit)
  systemctl_disable(unit)
  system_ok("systemctl", "mask", unit)
end

def systemctl_enable(unit)
  system_ok("systemctl", "daemon-reload")
  system_ok("systemctl", "enable", unit)
end

def systemd_add_service(name, command,
                        after: "multi-user.target",
                        before: nil,
                        wanted_by: "default.target",
                        type: "oneshot")
  if ! after; after_string = ""
  else after_string = "Requires=#{after}\nAfter=#{after}\n" end
  if ! before; before_string = ""
  else before_string = "RequiredBy=#{before}\nBefore=#{before}" end
  File.write("/etc/systemd/system/#{name}.service", <<_EOT_)
[Unit]
#{after_string}#{before_string}
[Service]
Type=#{type}
ExecStart=#{command}
[Install]
WantedBy=#{wanted_by}
_EOT_
  systemctl_enable("#{name}.service")
end

def systemd_add_timer(name, command, interval: nil, user: "root")
  File.write("/etc/systemd/system/#{name}.timer", <<_EOT_)
[Timer]
OnBootSec=1m
OnUnitActiveSec=#{interval}

[Install]
WantedBy=timers.target
_EOT_
  File.write("/etc/systemd/system/#{name}.service", <<_EOT_)
[Service]
Type=oneshot
User=#{user}
ExecStart=#{command}
_EOT_
  systemctl_enable("#{name}.timer")
end

def cron_add(schedule, command, crontab: "/etc/crontab", user: "root")
  unless schedule.split(" ").length == 5
    puts "fatal: malformed cron schedule: #{schedule}"
    exit 100
  end
  File.readlines(crontab).each do |line|
    c = line.split(/\s+/, 7)[6]
    if command == c; return nil end
  end
  append_file_ifhasnotline(crontab, "#{schedule} #{user} #{command}")
end

class IptablesChain
  def initialize(ref_chain, table: "filter", policy: "DROP")
    @table = table
    @chain = "#{ref_chain}_my"
    system_ok("iptables", "-t", @table, "-P", ref_chain, policy)
    system_try("iptables", "-t", @table, "-D", ref_chain, "-j", @chain)
    system_try("iptables", "-t", @table, "-F", @chain)
    system_try("iptables", "-t", @table, "-X", @chain)
    system_ok("iptables", "-t", @table, "-N", @chain)
    system_ok("iptables", "-t", @table, "-A", ref_chain, "-j", @chain)
  end

  def add(*args)
    system_ok("iptables", "-t", @table, "-A", @chain, *args)
  end

  def antispoof(spec_list)
    spec_list.each do |spec|
      add("-i", spec[:device], "!", "-s", spec[:subnet], "-j", "REJECT")
    end
  end

  def add_conntrack
    add("-m", "conntrack", "--ctstate", "ESTABLISHED,RELATED", "-j", "ACCEPT")
  end

  def allow_in_lo
    add("-i", "lo", "-j", "ACCEPT")
  end

  def allow_ping
    add("-p", "icmp", "--icmp-type", "echo-request", "-j", "ACCEPT")
  end

  def allow_dport(*port, source: "0.0.0.0/0", polarity: true)
    if polarity; neg = [] else neg = ["!"] end
    port.each do |p|
      add(*neg, "-s", source, "-p", "tcp", "--dport", p.to_s,
          "-m", "conntrack", "--ctstate", "NEW", "-j", "ACCEPT")
    end
  end

  def reject_all
    add("-j", "REJECT")
  end
end

class IptablesChainInput < IptablesChain
  def initialize
    super "INPUT"
    add_conntrack
    allow_in_lo
    allow_ping
  end
end

class IptablesChainForward < IptablesChain
  def initialize
    super "FORWARD"
    add_conntrack
    allow_in_lo
  end
end

def useradd(user, *opts, var: false)
  x = Etc.getpwnam(user) rescue false
  if var
    base = "/var/opt/#{MY_NAMESPACE}/#{MY_NAME}"
    FileUtils.mkdir_p(base)
    opts = opts + ["-b", base]
  end
  system_ok("useradd", "-m", "-s", "/bin/bash", *opts, user) unless x
end

def userdel(user)
  x = Etc.getpwnam(user) rescue false
  system_ok("userdel", "-r", user) if x
end

def chpasswd(user, passwd)
  pipe_into("#{user}:#{passwd}\n", "chpasswd")
end

def grub_password_hash(password)
  pipe_into("#{password}\n#{password}\n", "grub-mkpasswd-pbkdf2").split("\n")[2].split(" ")[6]
end

def apt_get_clean; system_ok("apt-get", "-y", "clean") end
def apt_get_update; system_ok("apt-get", "-y", "update") end
def apt_get_upgrade; system_ok("apt-get", "-y", "dist-upgrade") end

def apt_get_install(*args, target_release: nil)
  if target_release
    system_ok("apt-get", "-y", "-t", target_release, "install", *args)
  else
    system_ok("apt-get", "-y", "install", *args)
  end
  apt_get_language_support
  disable_gnome_keyring
  disable_gnome_tracker
end

def apt_get_install_from_file(list_file)
  apt_get_install(*File.readlines(list_file).map(&:chomp))
end

def apt_get_install_norec(*args)
  system_ok("apt-get", "-y", "install", "--no-install-recommends", *args)
end

def apt_get_reinstall(*args)
  system_ok("apt-get", "-y", "install", "--reinstall", *args)
end

def apt_get_remove(*args)
  args.each do |p|
    if system_try("dpkg", "-l", p)
      system_ok("apt-get", "-y", "remove", "--purge", p)
    end
  end
end

def apt_get_autoremove
  system_ok("apt-get", "-y", "autoremove")
end

def apt_add_repository(repos)
  system_ok("add-apt-repository", "-y", "-u", repos)
end

def apt_get_language_support
  if find_executable("check-language-support")
    package_list = system_capture("check-language-support").split(" ")
    apt_get_install(*package_list)
  end
end

def apt_add_area(area)
  process_file_ln("/etc/apt/sources.list") do |f,l|
    if l.match(/^(deb|deb-src)\s+/) && ! l.match(/\s#{area}(\s|$)/)
      f.puts "#{l} #{area}"
    else
      f.puts l
    end
  end
end

def flatpak_install(*args)
  system_ok("flatpak", "install", "-y", "flathub", *args)
end

def flatpak_install_from_file(list_file)
  flatpak_install(*File.readlines(list_file).map(&:chomp))
end

def disable_file(filename)
  FileUtils.mkdir_p("/root/diverted")
  if File.exist?(filename)
    x = File.basename(filename)
    system_ok("dpkg-divert", "--divert", "/root/diverted/#{x}", "--rename", filename)
  end
end

def debconf(config)
  pipe_into("#{config}\n", "debconf-set-selections")
end

def set_option_space(filename, option, value, comment: "#*")
  process_file(filename) do |c|
    c.gsub!(/^\s*#{comment}\s*#{option}\s+.*$/, "#{option} #{value}")
  end
end

def set_option_equals(filename, option, value, comment: "#*")
  process_file(filename) do |c|
    c.gsub!(/^\s*#{comment}\s*#{option}=.*$/, "#{option}=#{value}")
  end
end

def set_option_equals_spaces(filename, option, value, comment: "#*")
  process_file(filename) do |c|
    c.gsub!(/^\s*#{comment}\s*#{option}\s+=\s+.*$/, "#{option} = #{value}")
  end
end

def set_option_colon(filename, option, value, comment: "#*")
  process_file(filename) do |c|
    c.gsub!(/^\s*#{comment}\s*#{option}:\s+.*$/, "#{option}: #{value}")
  end
end

def uncomment_line(filename, regexp, comment: "#+")
  process_file_ln(filename) do |f, l|
    if l.match(regexp); l.gsub!(/^\s*#{comment}\s*(.*)$/, '\1') end
    f.puts(l)
  end
end

def add_mail_alias(src, dst)
  added = false
  process_file_ln("/etc/aliases") do |f, l|
    if l.match(/^#{src}:\s+/) && ! l.match(/#{dst}/)
      f.puts(l + ", #{dst}")
      added = true
    else
      f.puts(l)
    end
  end
  unless added
    append_file("/etc/aliases", src + ": " + dst + "\n")
  end
  system_ok("newaliases")
end

def write_my_etc(field, content, prefix: "")
  filename = "#{prefix}/#{MY_ETC}/#{field}"
  FileUtils.mkdir_p(File.dirname(filename))
  File.write(filename, Array(content).join("\n") + "\n")
end

def read_my_etc(field, prefix: "", default: nil)
  filename = "#{prefix}/#{MY_ETC}/#{field}"
  unless File.exist?(filename)
    if default.nil?
      puts "fatal: unable to read my etc: ${field}"
      exit 111
    else
      return default
    end
  end
  a = File.readlines(filename).map(&:chomp)
  if a.length == 1; a[0] else a end
end

def sftp_read_my_etc(host, field, opts={})
  filename = "/#{MY_ETC}/#{field}"
  opts.merge!(verify_host_key: :always)
  Net::SFTP.start(host, "root", opts) do |sftp|
    sftp.file.open(filename) do |file|
      a = file.read.split("\n")
      if a.length == 1; return a[0] else return a end
    end
  end
end

def get_from_profile(key, profile: nil)
  profile_ = profile.nil? ? read_my_etc("profile") : profile
  if ! PROFILE_DEF.keys.include?(profile_) || ! PROFILE_DEF[profile_].keys.include?(key)
    puts "fatal: unable to get #{key} from profile #{profile}"
    exit 111
  end
  PROFILE_DEF[profile_][key]
end

def ssh_keygen(file: nil, force: false, opts: [])
  file_ = file ? file : "#{home_dir}/.ssh/id_rsa"
  if ! File.exist?(file_) || force
    opts = opts + ["-f", file_]
    system_ok("ssh-keygen", "-t", "rsa", "-b", "4096", "-N", "", *opts)
    return file_
  end
  false
end

def authorized_keys_line(keyfile, command: nil)
  res = ""
  unless ! command
    res = "command=\"#{command}\",no-port-forwarding,no-agent-forwarding,no-X11-forwarding,no-pty "
  end
  res = res + File.read(keyfile)
  unchomp(res)
end

def add_authorized_key(user, keyfile, command: nil)
  append_file(File.expand_path("~#{user}/.ssh/authorized_keys"),
              authorized_keys_line(keyfile, command: command))
end

## zfs

def zfs_get(property, dataset)
  system_capture("/sbin/zfs", "get", "-H", property, dataset).split(" ")[2]
end

def zfs_remount(dataset)
  system_try("zfs", "umount", dataset)
  system_ok("zfs", "mount", dataset)
end

def zfs_mounted_at?(dataset, mountpoint)
  zfs_get("mounted", dataset) == "yes" &&
    zfs_get("mountpoint", dataset) == mountpoint &&
    File.directory?("#{mountpoint}/.zfs/snapshot")
end

def zfs_home_mounted?(username)
  zfs_mounted_at?("pool1/home/#{username}", "/home/#{username}")
end

## ldap

class MyLDAP
  def initialize
    password = File.read("/etc/ldapscripts/ldapscripts.passwd").chomp
    @conn = Net::LDAP.new(
      { host: HOST_SRV1,
        port: 389,
        auth: { method: :simple,
                username: "cn=admin,#{LDAP_DOMAIN_DN}",
                password: password,
              },
        encryption: { method: :start_tls },
      })
  end

  def ls
    ["Groups", "Users"].each do |ou|
      @conn.search(base: "ou=#{ou},#{LDAP_DOMAIN_DN}").each do |ent|
        p ent
      end
      puts
    end
    nil
  end

  def groupnames
    res = @conn.search(base: "ou=Groups,#{LDAP_DOMAIN_DN}").collect do |ent|
      if ent.objectclass.include?("posixGroup")
        ent.cn[0]
      else
        nil
      end
    end
    res.compact
  end

  def search_1(field, value, partial_base)
    filter = Net::LDAP::Filter.eq(field, value)
    ents = @conn.search(base: "#{partial_base},#{LDAP_DOMAIN_DN}", filter: filter)
    if ents.length > 1
      puts "fatal: MyLDAP::search_1: more than one entry"
      exit 111
    end
    ents[0]
  end

  def user(arg)
    case arg
    when String then search_1("uid", arg, "ou=Users")
    when Integer then search_1("uidNumber", arg.to_s, "ou=Users")
    end
  end

  def group(arg)
    case arg
    when String then search_1("cn", arg, "ou=Groups")
    when Integer then search_1("gidNumber", arg.to_s, "ou=Groups")
    end
  end

  def user_dn(username)
    "uid=#{username},ou=Users,#{LDAP_DOMAIN_DN}"
  end

  def group_dn(groupname)
    "cn=#{groupname},ou=Groups,#{LDAP_DOMAIN_DN}"
  end

  def add_user(username, uidnumber, groupname)
    gidnumber = group(groupname).gidnumber[0]
    password = cryptpw(SecureRandom.hex(32))
    attr = {
      objectClass: ["account", "posixAccount"],
      cn: username,
      uid: username,
      uidNumber: uidnumber.to_s,
      gidNumber: gidnumber,
      homeDirectory: "/home/#{username}",
      loginShell: "/bin/bash",
      gecos: username,
      description: "User account",
      userPassword: password,
    }
    @conn.add(dn: user_dn(username), attributes: attr)
  end

  def delete_user(username)
    if username.empty? || ! user(username); return nil end
    kill_all(username)
    leave_all_groups(username)
    if user(username).homedirectory[0] == "/home/#{username}" && zfs_home_mounted?(username)
      Dir.glob("/home/#{username}/.zfs/snapshot/*").each do |path|
        snapshot = File.basename(path)
        system_ok("zfs", "destroy", "pool1/home/#{username}@#{snapshot}")
      end
      system_ok("zfs", "destroy", "pool1/home/#{username}")
      FileUtils.rmdir("/home/#{username}") if File.directory?("/home/#{username}")
    end
    @conn.delete(dn: user_dn(username))
  end

  def add_group(groupname, gidnumber)
    attr = {
      objectClass: "posixGroup",
      cn: groupname,
      gidNumber: gidnumber.to_s,
      description: "Group account",
    }
    @conn.add(dn: group_dn(groupname), attributes: attr)
  end

  def change_primary_group(username, groupname)
    gidnumber = group(groupname).gidnumber[0]
    @conn.replace_attribute(user_dn(username), :gidNumber, gidnumber)
  end

  def get_primary_gidnumber(username)
    user(username).gidnumber[0]
  end

  def members(groupname)
    group = group(groupname)
    if ! group.respond_to?(:memberuid) || ! group.memberuid || group.memberuid.empty?
      []
    else
      group.memberuid
    end
  end

  def member_of?(username, groupname)
    members(groupname).include?(username)
  end

  def enter_group(username, groupname)
    unless member_of?(username, groupname)
      @conn.add_attribute(group_dn(groupname), :memberUid, username)
    end
  end

  def leave_group(username, groupname)
    if member_of?(username, groupname)
      @conn.replace_attribute(group_dn(groupname), :memberUid, members(groupname) - [username])
    end
  end

  def leave_all_groups(username)
    groupnames.each { |groupname| leave_group(username, groupname) }
  end

  def set_shell(username, shell)
    @conn.replace_attribute(user_dn(username), :loginShell, shell)
  end

  def get_shell(username)
    user(username).loginshell[0]
  end

  def set_password(username, password, plaintext: false)
    if plaintext
      password = cryptpw(password)
    end
    @conn.replace_attribute(user_dn(username), :userPassword, password)
  end

  def get_password(username)
    user(username).userpassword[0]
  end

  def set_password_hash(username, hash)
    pw = get_password(username).split("$")
    pw[4] = hash
    set_password(username, pw.join("$"))
  end

  def get_password_hash(username)
    pw = get_password(username).split("$")
    pw[4]
  end

  def user_locked?(username)
    get_shell(username) == "/bin/false" &&
      get_primary_gidnumber(username) == LDAP_LOCKED_GROUP[:gidNumber].to_s &&
      get_password_hash(username).start_with?("!")
  end

  def lock_user(username)
    unless user_locked?(username)
      set_shell(username, "/bin/false")
      change_primary_group(username, LDAP_LOCKED_GROUP[:groupname])
      set_password_hash(username, "!" + get_password_hash(username))
    end
  end

  def unlock_user(username)
    if user_locked?(username)
      set_shell(username, "/bin/bash")
      change_primary_group(username, LDAP_PRIMARY_GROUP[:groupname])
      set_password_hash(username, get_password_hash(username).delete_prefix("!"))
    end
  end
end

## must config all

class BlockManagerBase
  def initialize
    @tags = Array(read_my_etc("tags"))
    @home = Dir.pwd
  end
  def block(**opts, &block)
    opts = { tag: "production" }.merge(opts)
    doit(opts, &block)
  end
  def doit(opts)
    if ready?(opts)
      ts = Time.now.strftime("%Y-%m-%d %H:%M:%S")
      puts "--- strt #{id_string(opts)} at #{ts} ---"
      yield
      ts = Time.now.strftime("%Y-%m-%d %H:%M:%S")
      FileUtils.touch(mark_filename(opts))
      puts "--- done #{id_string(opts)} at #{ts} ---"
    end
  end
end

class BlockManagerOnetime < BlockManagerBase
  def initialize
    super
    @count = 0
  end
  def formatted_count(opts)
    "%05d" % @count
  end
  def id_string(opts)
    formatted_count(opts)
  end
  def mark_filename(opts)
    "#{@home}/onetime-mark-#{formatted_count(opts)}"
  end
  def ready?(opts)
    @tags.include?(opts[:tag]) && (! File.exist?(mark_filename(opts)) || opts[:repeat])
  end
  def doit(**opts)
    @count += 1
    super(opts)
  end
end

class BlockManagerRegular < BlockManagerBase
  def id_string(opts)
    opts[:id]
  end
  def mark_filename(opts)
    "#{@home}/regular-mark-#{opts[:id]}"
  end
  def ready?(opts)
    if ! @tags.include?(opts[:tag]); return false end
    if File.exist?(mark_filename(opts)) && opts[:interval]
      if Time.now - ActiveSupport::Duration.parse(opts[:interval]) > File.mtime(mark_filename(opts)); return true
      else return false end
    end
    true
  end
end

## misc

def gui_dialog(type, title, text)
  if find_executable("kdialog")
    case type
    when :info then switch = "--msgbox"
    when :error then switch = "--error"
    end
    system_ok("kdialog", "--title", title, switch, text)
  else
    case type
    when :info then switch = "--info"
    when :error then switch = "--error"
    end
    system_ok("zenity", "--width", "500", switch, "--title", title, "--text", text)
  end
end

def unchomp(str)
  if str.empty?; return "\n" end
  if str[-1] == "\n"; return str end
  str + "\n"
end

def summarize_numbers(arr)
  if arr.length == 0
    ""
  elsif arr.length == 1
    arr[0].to_s
  else
    s = arr[0]
    t = s
    i = 1
    while i < arr.length && arr[i] == t+1
      t = arr[i]
      i += 1
    end
    if i == 1
      x = s
    elsif i == 2
      x = "#{s},#{t}"
    else
      x = "#{s}-#{t}"
    end
    y = summarize_numbers(arr[i, arr.length])
    if y != ""
      "#{x},#{y}"
    else
      x
    end
  end
end
