def main_network_device
  ip_link_output = system_capture("ip link")
  ip_link_output.each_line do |line|
    if line.include?("UP") && ! (line.include?("LOOPBACK") || line.include?("NO-CARRIER"))
      return line.split(": ")[1]
    end
  end
  puts "Could not deteremine main network device. Aborting."
  exit 111
end

def configure_network(device, profile: "default")
  ip_config_temp = get_from_profile(:ip_config_temp, profile: profile)
  gateway = get_from_profile(:gateway, profile: profile)
  systemctl_stop("network-manager.service")
  system "ip addr del dev #{device} #{ip_config_temp}", out: File::NULL, err: File::NULL
  system "ip route del default via #{gateway}", out: File::NULL, err: File::NULL
  system_ok("ip addr add dev #{device} #{ip_config_temp}")
  system_ok("ip route add default via #{gateway}")
  NAMESERVERS.each { |ip| append_file("/etc/resolv.conf", "nameserver #{ip}\n") }
  system "ping -c1 #{gateway}", out: File::NULL, err: File::NULL
  unless $?.success?
    puts "Could not configure network. Aborting."
    exit 111
  end
end

def ip_address_split(ip)
  s = ip.split(".")
  [s[0..2].join("."), s[3]]
end

def client_ip_address(mac_address, profile)
  Net::SFTP.start(HOST_SRV1, USER_LOG, verify_host_key: :always) do |sftp|
    sftp.dir.foreach("client") do |entry|
      s = ip_address_split(entry.name)
      profile_prefix = get_from_profile(:subnet_prefix, profile: profile)
      profile_min = get_from_profile(:subnet_suffix_min, profile: profile)
      profile_max = get_from_profile(:subnet_suffix_max, profile: profile)
      if (s[0] == profile_prefix) && (profile_min <= s[1].to_i) && (s[1].to_i <= profile_max)
        mac_filename = "client/#{entry.name}/mac"
        if sftp_stat_ok?(sftp, mac_filename)
          mac_address_server = sftp.file.open(mac_filename) { |file| file.read.chomp }
          return entry.name if mac_address == mac_address_server
        end
      end
    end
  end
  false
end

def backup_log(ip_address)
  ts = Time.now.strftime("%Y-%m-%d::%H:%M:%S")
  Net::SFTP.start(HOST_SRV1, USER_LOG, verify_host_key: :always) do |sftp|
    ["onetime", "regular"].each do |type|
      log_filename = "client/#{ip_address}/log-#{type}"
      if sftp_stat_ok?(sftp, log_filename)
        sftp.rename!(log_filename, "#{log_filename}.#{ts}")
      end
    end
  end
end

def next_free_number(profile)
  Net::SFTP.start(HOST_SRV1, USER_LOG, verify_host_key: :always) do |sftp|
    numbers = sftp.dir.entries("client").select do |entry|
      ! [".", ".."].include?(entry.name) && ip_address_split(entry.name)[0] == get_from_profile(:subnet_prefix, profile: profile)
    end.map { |entry| ip_address_split(entry.name)[1].to_i }
    (get_from_profile(:subnet_suffix_min, profile: profile)..get_from_profile(:subnet_suffix_max, profile: profile)).each do |n|
      unless numbers.include?(n); return n end
    end
  end
  puts "All IP addresses are taken. Aborting."
  exit 111
end

def register_client(mac_address, ip_address)
  Net::SFTP.start(HOST_SRV1, USER_LOG, verify_host_key: :always) do |sftp|
    dir = "client/#{ip_address}"
    sftp.mkdir!(dir)
    sftp.file.open("#{dir}/mac", "w") { |file| file.write("#{mac_address}\n") }
  end
end

def upload_known_hosts(ip_address, pubkey_file)
  Net::SFTP.start(HOST_SRV1, USER_LOG, verify_host_key: :always) do |sftp|
    dir = "client/#{ip_address}"
    sftp.file.open("#{dir}/known_hosts", "w") do |file|
      file.write("#{ip_address} ")
      file.write(File.read(pubkey_file))
    end
  end
end

def sftp_add_authorized_key(host, user, keyfile, command: nil)
  sftp_append_file(host, user, ".ssh/authorized_keys", authorized_keys_line(keyfile, command: command))
end

def unpack_tar(partition, distro, name)
  system_ok("mount #{partition} /mnt")
  system_ok("ssh #{SSH_DIS} 'cat #{distro}_#{name}.tar.xz' | xz -d | pv | tar -xp -C /mnt")
  yield if block_given?
  system_ok("umount /mnt")
end

def zero_device(device, try=false)
  cmd = "dd if=/dev/zero of=#{device} bs=1M count=499"
  if try
    system_try(cmd)
  else
    system_ok(cmd)
  end
end

def install_system(device, partition_prefix, distro)
  Dir.glob("#{device}#{partition_prefix}[1-9]").each { |d| zero_device(d, true) }
  zero_device(device)
  system_ok("parted #{device} mklabel gpt")
  system_ok("parted -a optimal #{device} mkpart primary fat32 1MiB 500MiB") # 1: EFI
  system_ok("parted #{device} set 1 esp on")
  system_ok("parted -a optimal #{device} mkpart primary ext4 500MiB 80GiB") # 2: /
  system_ok("parted -a optimal #{device} mkpart primary 80GiB 90GiB")       # 3: swap (crypted)
  system_ok("parted -a optimal #{device} mkpart primary 90GiB 100%")        # 4: /tmp (crypted)
  system_ok("parted #{device} name 1 boot")
  system_ok("parted #{device} name 2 root")
  system_ok("parted #{device} name 3 cryptoswap")
  system_ok("parted #{device} name 4 cryptotmp")
  sleep(5)
  system_ok("mkfs.vfat -F 32 #{device}#{partition_prefix}1")
  system_ok("mkfs.ext4 -F #{device}#{partition_prefix}2")
  boot_uuid = filesystem_uuid("#{device}#{partition_prefix}1")
  root_uuid = filesystem_uuid("#{device}#{partition_prefix}2")
  unpack_tar("#{device}#{partition_prefix}1", distro, "boot") do
    ["ubuntu", "debian"].each do |name|
      grub_cfg = "/mnt/EFI/#{name}/grub.cfg"
      if File.exist?(grub_cfg)
        process_file(grub_cfg) do |c|
          c.sub!(/^search.fs_uuid [^\s]* /, "search.fs_uuid #{root_uuid} ")
        end
      end
    end
  end
  unpack_tar("#{device}#{partition_prefix}2", distro, "root") do
    process_file_ln("/mnt/etc/fstab") do |f, l|
      if l.match(/vfat/); l.sub!(/^UUID=[^\s]* /, "UUID=#{boot_uuid} ") end
      if l.match(/ext4/); l.sub!(/^UUID=[^\s]* /, "UUID=#{root_uuid} ") end
      unless l.match(/swap/); f.puts(l) end
    end
    append_file("/mnt/etc/crypttab", "swap #{device}#{partition_prefix}3 /dev/urandom swap,cipher=aes-xts-plain,size=256\n")
    append_file("/mnt/etc/fstab", "/dev/mapper/swap none swap sw 0 0\n")
    append_file("/mnt/etc/crypttab", "tmp #{device}#{partition_prefix}4 /dev/urandom tmp,cipher=aes-xts-plain,size=256\n")
    append_file("/mnt/etc/fstab", "/dev/mapper/tmp /tmp ext4 defaults,noatime 0 0\n")
    process_file("/mnt/etc/apt/sources.list") do |c|
      c.gsub!(/^deb cdrom:.*$/, '# \0')
    end
    FileUtils.mv("/mnt/etc/resolv.conf", "/root/resolv.conf")
    NAMESERVERS.each { |ip| append_file("/mnt/etc/resolv.conf", "nameserver #{ip}\n") }
    ["dev", "dev/pts", "proc", "run", "sys"].each { |x| system_ok("mount -o bind /#{x} /mnt/#{x}") }
    system_ok("chroot /mnt mount -o umask=0077 #{device}#{partition_prefix}1 /boot/efi")
    system_ok("chroot /mnt apt-get -y update")
    MY_DEPENDS.each { |pkg| system_ok("chroot /mnt apt-get -y install #{pkg}") }
    system_ok("chroot /mnt apt-get -y install git netplan.io")
    system_ok("chroot /mnt apt-get -y install openssh-server")
    system_ok("chroot /mnt apt-get -y install grub-pc-bin")
    system_ok("chroot /mnt rm -f /etc/ssh/ssh_host_*")
    system_ok("chroot /mnt dpkg-reconfigure openssh-server")
    system_ok("chroot /mnt grub-install #{device}")
    system_ok("chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg")
    system_ok("chroot /mnt systemctl enable systemd-resolved.service")
    system_ok("chroot /mnt umount /boot/efi")
    ["dev/pts", "dev", "proc", "run", "sys"].each { |x| system_ok("umount -lf /mnt/#{x}") }
    FileUtils.rm("/mnt/etc/resolv.conf")
    FileUtils.mv("/root/resolv.conf", "/mnt/etc/resolv.conf")
    unless File.stat("/mnt/etc/resolv.conf").symlink?
      FileUtils.rm("/mnt/etc/resolv.conf")
      FileUtils.ln_s("/run/systemd/resolve/resolv.conf", "/mnt/etc/resolv.conf")
    end
  end
end
