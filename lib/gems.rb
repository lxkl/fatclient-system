require "net/sftp"
require "net/ssh"
require "net/ldap"
require "net/smtp"
require "socket"
require "fileutils"
require "pathname"
require "optparse"
require "mkmf"
require "etc"
require "time"
require "date"
require "digest"
require "securerandom"
require "sqlite3"
require "active_record"
require "active_support"
require "active_support/core_ext"
require "erb"

module MakeMakefile::Logging
  @logfile = File::NULL
  @quiet = true
end
