# coding: utf-8
$VERBOSE = true
require_relative "../lib/gems"
require_relative "../lib/defs"
require_relative "../lib/class_user"
require_relative "../lib/web_app_helpers"
require "sinatra"
require "sass"
require "gitlab_chronic_duration"

require_relative "connection"

set :sass, { :cache => false } # seems to be ineffective
enable :sessions
set :session_secret, SecureRandom.hex(64)

helpers WebAppHelpersCommon
helpers WebAppHelpers

before do
  session[:csrf_token] ||= SecureRandom.hex(64)
  session[:lang] ||= "en"
  if ! request.safe?
    halt(403, "CSRF protection failed.") unless session[:csrf_token] == params[:csrf_token]
  end
end

get "/styles.css" do
  scss :styles
end

get %r{/lang/(en|de)} do |l|
  session[:lang] = l
  redirect to request.referrer
end

get %r{/(en|de)/(.*)} do |l, r|
  session[:lang] = l
  redirect to "/" + r
end

get "/" do
  erb :index
end

get "/legal_notice" do
  erb :legal_notice
end

get "/register" do
  erb :register
end

post "/register" do
  @error_title = { "en" => "Registration", "de" => "Registrierung" }
  unless (@error_message = register_validate_userdata) == true
    return erb :error
  end
  @password = randompw
  now = Time.now
  begin
    @user = User.create({ "_groupname" => USERDB_DEFAULT_GROUP_NAME_REGISTER,
                          "_password" => cryptpw(@password),
                          "_activated" => false,
                          "_manual_activation_code" => SecureRandom.hex(4),
                          "_first_name" => params[:first_name],
                          "_last_name" => params[:last_name],
                          "_email" => params[:email],
                          "_created_at" => now,
                          "_renewed_at" => now,
                        })
  rescue => e
    @error_message = "Database error: #{e}"
    return erb :error
  end
  unless @user.valid?
    @error_message = @user.errors.messages.to_s
    return erb :error
  end
  erb :register_result
end

get "/activate/:id/:code" do
  @error_title = { "en" => "Activation", "de" => "Aktivierung" }
  user = User.find_by(id: params[:id])
  if user &&
     user.activation_code == params[:code] &&
     user.activateable?
    return erb :activate_input
  end
  @error_message = USERDB_ERROR_MESSAGE_CODE
  erb :error
end

post "/activate/:id/:code" do
  @error_title = { "en" => "Activation", "de" => "Aktivierung" }
  begin
    user = User.find_by(id: params[:id])
    if user &&
       user.activation_code == params[:code] &&
       user.manual_activation_code == params[:manual_activation_code] &&
       user.activateable?
      user["_activated"] = true
      user["_activation_code"] = nil
      user["_activation_code_sent"] = false
      user["_manual_activation_code"] = nil
      user.save
      return erb :activate_result
    end
  rescue ActiveRecord::StaleObjectError => e
    retry
  rescue => e
    @error_message = "Database error: #{e}"
    return erb :error
  end
  @error_message = USERDB_ERROR_MESSAGE_CODE
  erb :error
end

get "/renew/:id/:code" do
  @error_title = { "en" => "Renewal", "de" => "Verlängerung" }
  begin
    user = User.find_by(id: params[:id])
    if user &&
       user.renewal_code == params[:code] &&
       user.renewable?
      user["_renewed_at"] = Time.now
      user["_renewal_code"] = nil
      user["_renewal_code_sent"] = false
      user.save
      return erb :renew_result
    end
  rescue ActiveRecord::StaleObjectError => e
    retry
  rescue => e
    @error_message = "Database error: #{e}"
    return erb :error
  end
  @error_message = USERDB_ERROR_MESSAGE_CODE
  erb :error
end

get "/login" do
  erb :login
end

post "/login" do
  session[:admin] = authorization?(params[:password])
  erb :login_result
end

get "/logout" do
  session.clear
  erb :logout_result
end

get "/adduser" do
  redirect to "/login" unless session[:admin]
  @user = nil
  erb :adduser
end

post "/adduser" do
  @error_title = "Add user"
  redirect to "/login" unless session[:admin]
  unless (@error_message = adduser_edituser_validate_userdata) == true
    return erb :error
  end
  unless params[:terms_of_use] == "on"
    @error_message = "The terms of use (including the data protection statement) must be accepted."
    return erb :error
  end
  @password = randompw
  now = Time.now
  begin
    @user = User.create({ "_username" => params[:username],
                          "_groupname" => params[:groupname],
                          "_password" => cryptpw(@password),
                          "_activated" => true,
                          "_first_name" => params[:first_name],
                          "_last_name" => params[:last_name],
                          "_email" => params[:email],
                          "_quota_block" => params[:quota_block],
                          "_quota_inode" => params[:quota_inode],
                          "_valid_for" => params[:valid_for],
                          "_created_at" => now,
                          "_renewed_at" => now,
                        })
  rescue => e
    @error_message = "Database error: #{e}"
    return erb :error
  end
  unless @user.valid?
    @error_message = @user.errors.messages.to_s
    return erb :error
  end
  erb :adduser_result
end

get "/edituser" do
  redirect to "/login" unless session[:admin]
  erb :edituser_select
end

post "/edituser" do
  @error_title = "Edit user"
  redirect to "/login" unless session[:admin]
  params[:username_or_email].strip!
  params[:username_or_email].downcase!
  if email_valid?(params[:username_or_email])
    @user = User.all.select{ |user| user.email == params[:username_or_email] }[0]
  else
    @user = User.all.select{ |user| user.username == params[:username_or_email] }[0]
  end
  unless @user
    @error_message = "Not found: <code>#{params[:username_or_email]}</code>."
    return erb :error
  end
  erb :edituser_edit
end

post "/edituser/:id" do
  @error_title = "Edit user"
  redirect to "/login" unless session[:admin]
  unless (@error_message = adduser_edituser_validate_userdata) == true
    return erb :error
  end
  begin
    @user = User.update(params[:id],
                        { "_groupname" => params[:groupname],
                          "_first_name" => params[:first_name],
                          "_last_name" => params[:last_name],
                          "_email" => params[:email],
                          "_quota_block" => params[:quota_block],
                          "_quota_inode" => params[:quota_inode],
                          "_valid_for" => params[:valid_for],
                          "_barred" => (params[:barred] == "on"),
                        })
    if params[:renew_now] == "on"
      User.update(params[:id],
                  { "_renewed_at" => Time.now,
                    "_renewal_code" => nil,
                    "_renewal_code_sent" => false,
                  })
    end
  rescue => e
    @error_message = "Database error: #{e}"
    return erb :error
  end
  unless @user.valid?
    @error_message = @user.errors.messages.to_s
    return erb :error
  end
  @retrieved_user = User.find(params[:id])
  @retrieved_user_attrs = @retrieved_user.attributes.to_h
  erb :edituser_result
end

not_found do
  erb :not_found
end
